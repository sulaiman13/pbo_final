<?php

namespace App\Http\Controllers\App\Master;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use App\Model\App\Master\Value;
use App\Model\App\Master\Student as Model;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
class MyStudentController extends Controller
{
    public function index () {
        $students = Model::where('assistant_id', Auth::user()->id)->orderBy('name','ASC')->get();
        return view('app.asisten.my_praktikan', compact('students'));
    }
    public function store(Request $request) {
        $model = new Model();
        $model->code = $request->get('code');
        $model->name = $request->get('name');
        $model->course_id = '53e53d78-baee-4cc1-8e2d-cba5063d7fe1';
        $model->assistant_id = Auth::user()->id;
        $model->created_by = Auth::user()->id;
        $model->update_by = Auth::user()->id;
        $model->save();
        return redirect()->route('app.master.myStudent.index');
    }
    public function detail(Model $student) {

        $returned = new \stdClass();
        $returned->body = new Collection();
        foreach (Value::where('student_id',$student->id)->orderBy('meeting','ASC')->get() as $value) {
          $dataBody = new \stdClass();
          $dataBody->id = $value->id;
          $dataBody->meeting = $value->meeting;
          $dataBody->responsValue = $value->responsValue;
          $dataBody->preliminaryAssignmentValue = $value->preliminaryAssignmentValue;
          $dataBody->practiceValue = $value->practiceValue;
          $returned->body->push($dataBody);
        }

        return view('app.asisten.studentDetail', compact('returned','student'));
    }
    public function update(Model $myStudent, Request $request) {
      $myStudent->name = $request['name'];
      $myStudent->code = $request['code'];
      $myStudent->save();
      return redirect()->route('app.master.myStudent.index');
    }
    public function destroy(Model $myStudent) {
      //dd(Value::all()->where('student_id', $myStudent->id));
      if($myStudent->delete())
      {
        $deletes = Value::all()->where('student_id', $myStudent->id);
        foreach ($deletes as $delete) {
          $delete->delete();
        }
      }
      return redirect()->route('app.master.myStudent.index');
    }
}

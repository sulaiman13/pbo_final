<?php

namespace App\Http\Controllers\App\Master;

use Illuminate\Http\Request;
use App\Model\App\Master\Value;
use App\Model\App\Master\Student as Model;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;

class RecapController extends Controller
{
    public function index() {

        $datas = new \stdClass();
        $datas->students = new Collection();
        foreach (Model::orderBy('code', 'ASC')->get() as $student) {
            $dataStudent = new \stdClass();
            $dataStudent->code = $student->code;
            $dataStudent->name = $student->name;
            $dataStudent->responsValue = Value::whereIn('student_id', [$student->id])->get()->sum('responsValue')/8*(20/100);
            $dataStudent->preliminaryAssignmentValue = Value::whereIn('student_id', [$student->id])->get()->sum('preliminaryAssignmentValue')/8*(30/100);
            $dataStudent->practiceValue = Value::whereIn('student_id', [$student->id])->get()->sum('practiceValue')/8*(50/100);
            $dataStudent->total = $dataStudent->responsValue + $dataStudent->preliminaryAssignmentValue + $dataStudent->practiceValue;
            $datas->students->push($dataStudent);
        }
        return view('app.asisten.recap', compact('datas'));
    }
}

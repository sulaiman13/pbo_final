@extends('layouts.mrx')
@section('head')
Praktikan Saya
<a href="#" class="pull-right text-black"  data-toggle="modal" data-target="#modal"><span class="glyphicon glyphicon-plus"></a>
  @endsection
  @section('content')
  <div class="row">
    @foreach ($students as $student)
    <div class="col-md-3">
      <!-- Widget: user widget style 1 -->
      <div class="box box-widget widget-user">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-mrx-red">
          <h3 class="widget-user-username text-black mrx-center is-name">{{$student->name}}</h3>
          <h5 class=" text-black mrx-center is-nim">{{$student->code}}</h5>
        </div>

        <div class="box-footer is-footer">
          <div class="pull-right">
            <button class="btn" type="button" name="button">
              <a href="#" data-toggle="modal" data-target="#modal_edit_{{$student->id}}">
                <span class="glyphicon glyphicon-pencil
                color-green"></span></a>
              </button>
            </div>
            <form class="" action="{{route('app.master.myStudent.destroy', $student->id)}}" method="post">
              {{ csrf_field()}}
              {{ method_field('DELETE')}}
              <div class="pull-left">
                <button class="btn glyphicon glyphicon-trash
                color-red" onclick="return confirm('Apakah Anda Yakin Ingin Menghapus Data ini?')"></button>
              </div>

            </form>
            <center>
              <a href="{{route('studentDetail',$student->id)}}">
                <button class="btn" type="button" name="button">
                  <span class="glyphicon glyphicon-th
                  color-yellow"></span>
                </button>
              </a>
            </center>


          </div>
        </div>
        <!-- /.widget-user -->
      </div>
      <div class="modal fade" id="modal_edit_{{$student->id}}">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title">Edit Praktikan</h4>
                      </div>
                      <div class="modal-body">
                          <form class="" action="{{ route('app.master.myStudent.update', $student->id)}}" method="post">
                              {{ csrf_field() }}
                              {{ method_field('PUT')}}
                              <label>Nama</label>
                              <input type="text" class="form-control" value="{{$student->name}}" name="name">
                              <label>Nim</label>
                              <input type="text" class="form-control" value="{{$student->code}}" name="code">
                              <label>Mata Kuliah</label>
                              <input disabled type="number" class="form-control"  name=course_id>
                          </div>
                          <div class="modal-footer">
                              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Simpan</button>
                          </div>
                      </form>
                  </div>
                  <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
          </div>

      @endforeach
      <div class="modal fade" id="modal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Praktikan</h4>
              </div>
              <div class="modal-body">
                <form class="" action="{{ route('app.master.myStudent.store')}}" method="post">
                  {{ csrf_field() }}
                  <label>Nama</label>
                  <input type="text" class="form-control" placeholder="Masukkan Nama Praktikan" name="name">
                  <label>Nim</label>
                  <input type="text" class="form-control" placeholder="Masukkan Nim Praktikan" name="code">
                  <label>Mata Kuliah</label>
                  <input disabled type="number" class="form-control" placeholder="Masukkan Nilai Praktikum" name=course_id>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      </div>

    </div>
    @endsection

@extends('layouts.mrx')
@section('head')
    {{$student->name}}
    <br>
    {{$student->code}}
    <a href="{{route('app.master.myStudent.index')}}" class="pull-right text-black"><span class="glyphicon glyphicon-chevron-left"></a>
@endsection
@section('content')
        <div class="modal fade" id="modal_{{$student->id}}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Input Nilai</h4>
                        </div>
                        <div class="modal-body">
                            <form class="" action="{{ route('app.master.value.store')}}" method="post">
                                {{ csrf_field() }}
                                <label>Pertemuan Ke</label>
                                <select class="form-control" name="meeting">
                                    <option value="1">Pertemuan 1</option>
                                    <option value="2">Pertemuan 2</option>
                                    <option value="3">Pertemuan 3</option>
                                    <option value="4">Pertemuan 4</option>
                                    <option value="5">Pertemuan 5</option>
                                    <option value="6">Pertemuan 6</option>
                                    <option value="7">Pertemuan 7</option>
                                    <option value="8">Pertemuan 8</option>
                                </select>
                                <label>Nilai Respon</label>
                                <input type="number" class="form-control" placeholder="Masukkan Nilai Respon" name="responsValue" value="0">
                                <label>Nilai TP</label>
                                <input type="number" class="form-control" placeholder="Masukkan Nilai TP" name="preliminaryAssignmentValue" value="0">
                              <label>Nilai Praktikum</label>
                                <input type="number" class="form-control" placeholder="Masukkan Nilai Praktikum" name="practiceValue" value="0">
                                <input type="hidden" name="student_id" value="{{$student->id}}">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        <div class="modal fade" id="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Tambah Praktikan</h4>
                        </div>
                        <div class="modal-body">
                            <form class="" action="{{ route('app.master.myStudent.store')}}" method="post">
                                {{ csrf_field() }}
                                <label>Nama</label>
                                <input type="text" class="form-control" placeholder="Masukkan Nama Praktikan" name="name">
                                <label>Nim</label>
                                <input type="text" class="form-control" placeholder="Masukkan Nim Praktikan" name="code">
                                <label>Mata Kuliah</label>
                                <input disabled type="number" class="form-control" placeholder="Masukkan Nilai Praktikum" name=course_id>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Tambah</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <div class="box">
                <div class="box-header with-border">
                    <a href="#" class="pull-right text-black" data-toggle="modal" data-target="#modal_{{$student->id}}"><span class="glyphicon glyphicon-plus"></a><br>
                    <table class="table table-bordered">
                        <tr>
                            <th style="width: 5px">#</th>
                            <th class="mrx-center " style="width: 30%">Nilai Respon</th>
                            <th class="mrx-center" style="width: 30%">Nilai TP</th>
                            <th class="mrx-center" style="width: 30%">Nilai Praktikum</th>
                            <th style="width: 5%">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </th>
                        </tr>
                        @foreach ($returned->body as $value)
                            <tr>
                                <td>{{$value->meeting}}</td>
                                <td>
                                    <div class="row">
                                        @if ($value->responsValue >= 0 && $value->responsValue <= 50)
                                            <div class="col-md-10">
                                                <div class="progress progress-xs progress-striped active">
                                                    <div class="progress-bar progress-bar-danger" style="width:  {{$value->responsValue}}%">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="pull-right">
                                                    <span class="badge bg-red">{{$value->responsValue}}</span>
                                                </div>
                                            </div>
                                        @elseif ($value->responsValue > 50 && $value->responsValue < 80)
                                            <div class="col-md-10">
                                                <div class="progress progress-xs progress-striped active">
                                                    <div class="progress-bar progress-bar-yellow" style="width:  {{$value->responsValue}}%">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="pull-right">
                                                    <span class="badge bg-yellow">{{$value->responsValue}}</span>
                                                </div>
                                            </div>
                                        @else
                                            <div class="col-md-10">
                                                <div class="progress progress-xs progress-striped active">
                                                    <div class="progress-bar progress-bar-green" style="width:  {{$value->responsValue}}%">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="pull-right">
                                                    <span class="badge bg-green">{{$value->responsValue}}</span>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                       @if ($value->preliminaryAssignmentValue >= 0 && $value->preliminaryAssignmentValue < 50)
                                           <div class="col-md-10">
                                               <div class="progress progress-xs progress-striped active">
                                                 <div class="progress-bar progress-bar-danger" style="width:  {{$value->preliminaryAssignmentValue}}%">
                                                 </div>
                                               </div>
                                           </div>
                                           <div class="col-md-2">
                                               <div class="pull-right">
                                                   <span class="badge bg-red">{{$value->preliminaryAssignmentValue}}</span>
                                               </div>
                                           </div>
                                       @elseif ($value->preliminaryAssignmentValue > 50 && $value->preliminaryAssignmentValue < 80)
                                           <div class="col-md-10">
                                               <div class="progress progress-xs progress-striped active">
                                                 <div class="progress-bar progress-bar-yellow" style="width:  {{$value->preliminaryAssignmentValue}}%">
                                                 </div>
                                               </div>
                                           </div>
                                           <div class="col-md-2">
                                               <div class="pull-right">
                                                   <span class="badge bg-yellow">{{$value->preliminaryAssignmentValue}}</span>
                                               </div>
                                           </div>
                                       @else
                                           <div class="col-md-10">
                                               <div class="progress progress-xs progress-striped active">
                                                 <div class="progress-bar progress-bar-green" style="width:  {{$value->preliminaryAssignmentValue}}%">
                                                 </div>
                                               </div>
                                           </div>
                                           <div class="col-md-2">
                                               <div class="pull-right">
                                                   <span class="badge bg-green">{{$value->preliminaryAssignmentValue}}</span>
                                               </div>
                                           </div>
                                       @endif
                                    </div>
                                </td>
                                <td>
                                    <div class="row">
                                       @if ($value->practiceValue >= 0 && $value->practiceValue < 50)
                                           <div class="col-md-10">
                                               <div class="progress progress-xs progress-striped active">
                                                 <div class="progress-bar progress-bar-danger" style="width:  {{$value->practiceValue}}%">
                                                 </div>
                                               </div>
                                           </div>
                                           <div class="col-md-2">
                                               <div class="pull-right">
                                                   <span class="badge bg-red">{{$value->practiceValue}}</span>
                                               </div>
                                           </div>
                                       @elseif ($value->practiceValue > 50 && $value->practiceValue < 80)
                                           <div class="col-md-10">
                                               <div class="progress progress-xs progress-striped active">
                                                 <div class="progress-bar progress-bar-yellow" style="width:  {{$value->practiceValue}}%">
                                                 </div>
                                               </div>
                                           </div>
                                           <div class="col-md-2">
                                               <div class="pull-right">
                                                   <span class="badge bg-yellow">{{$value->practiceValue}}</span>
                                               </div>
                                           </div>
                                       @else
                                           <div class="col-md-10">
                                               <div class="progress progress-xs progress-striped active">
                                                 <div class="progress-bar progress-bar-green" style="width:  {{$value->practiceValue}}%">
                                                 </div>
                                               </div>
                                           </div>
                                           <div class="col-md-2">
                                               <div class="pull-right">
                                                   <span class="badge bg-green">{{$value->practiceValue}}</span>
                                               </div>
                                           </div>
                                       @endif
                                   </div>
                                </td>
                                <td class="color-brown"><span class="glyphicon glyphicon-pencil"  data-toggle="modal" data-target="#edit_{{$value->id}}"></span></td>
                                <div class="modal fade" id="edit_{{$value->id}}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Edit Nilai</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form class="" action="{{ route('app.master.value.update', $value->id)}}" method="post">
                                                        {{ csrf_field() }}
                                                        {{ method_field('PUT') }}
                                                        <label>Pertemuan Ke</label>
                                                        <select class="form-control" name="meeting" selected="{{$value->meeting}}">
                                                            @for ($i=1; $i < 9; $i++)
                                                                <option value={{$i}}>Pertemuan {{$i}}</option>
                                                                @if ($i == $value->meeting)
                                                                    <option value={{$i}} selected>Pertemuan {{$i}}</option>
                                                                @endif
                                                            @endfor

                                                        </select>
                                                        <label>Nilai Respon</label>
                                                        <input type="number" class="form-control" placeholder="Masukkan Nilai Respon" name="responsValue" value="{{$value->responsValue}}">
                                                        <label>Nilai TP</label>
                                                        <input type="number" class="form-control" placeholder="Masukkan Nilai TP" name="preliminaryAssignmentValue" value="{{$value->preliminaryAssignmentValue}}">
                                                        <label>Nilai Praktikum</label>
                                                        <input type="number" class="form-control" placeholder="Masukkan Nilai Praktikum" name="practiceValue" value="{{$value->practiceValue}}">
                                                        <input type="hidden" name="student_id" value="{{$student->id}}">

                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Tambah</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>

@endsection

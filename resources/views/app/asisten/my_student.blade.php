@extends('layouts.mrx')
@section('head')
    Praktikan Saya V2
@endsection
@section('content')
    <a href="#" class="pull-right" data-toggle="modal" data-target="#modal"><span class="glyphicon glyphicon-plus"></a>
        <br><br>
        <div class="col">
            @foreach ($students as $student)
                <div class="modal fade" id="modal_{{$student->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Input Nilai</h4>
                                </div>
                                <div class="modal-body">
                                    <form class="" action="{{ route('app.master.value.store')}}" method="post">
                                        {{ csrf_field() }}
                                        <label>Pertemuan Ke</label>
                                        <select class="form-control" name="meeting">
                                            <option value="1">Pertemuan 1</option>
                                            <option value="2">Pertemuan 2</option>
                                            <option value="3">Pertemuan 3</option>
                                            <option value="4">Pertemuan 4</option>
                                            <option value="5">Pertemuan 5</option>
                                            <option value="6">Pertemuan 6</option>
                                            <option value="7">Pertemuan 7</option>
                                            <option value="8">Pertemuan 8</option>
                                        </select>
                                        <label>Nilai TP</label>
                                        <input type="number" class="form-control" placeholder="Masukkan Nilai TP" name="preliminaryAssignmentValue" value="0">
                                        <label>Nilai Respon</label>
                                        <input type="number" class="form-control" placeholder="Masukkan Nilai Respon" name="responsValue" value="0">
                                        <label>Nilai Praktikum</label>
                                        <input type="number" class="form-control" placeholder="Masukkan Nilai Praktikum" name="practiceValue" value="0">
                                        <input type="hidden" name="student_id" value="{{$student->id}}">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Tambah</button>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="box box-default collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{$student->name}} | {{$student->code}}</h3>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <span class="glyphicon glyphicon-plus pull-right" data-toggle="modal" data-target="#modal_{{$student->id}}"></span>
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Pertemuan Ke</th>
                                        <th>Nilai Respon</th>
                                        <th>Nilai TP</th>
                                        <th>Nilai Praktikum</th>
                                        <th>
                                            <span class="glyphicon glyphicon-pencil"></span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($student->values as $value)
                                        <tr>
                                            <td>{{$value->meeting}}</td>
                                            <td>{{$value->responsValue}}</td>
                                            <td>{{$value->preliminaryAssignmentValue}}</td>
                                            <td>{{$value->practiceValue}}</td>
                                            <td><span class="glyphicon glyphicon-pencil"  data-toggle="modal" data-target="#edit_{{$value->id}}"></span></td>
                                            <div class="modal fade" id="edit_{{$value->id}}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span></button>
                                                                <h4 class="modal-title">Edit Nilai</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form class="" action="{{ route('app.master.value.update', $value->id)}}" method="post">
                                                                    {{ csrf_field() }}
                                                                    {{ method_field('PUT') }}
                                                                    <label>Pertemuan Ke</label>
                                                                    <select class="form-control" name="meeting" selected="{{$value->meeting}}">
                                                                        @for ($i=1; $i < 9; $i++)
                                                                            <option value={{$i}}>Pertemuan {{$i}}</option>
                                                                            @if ($i == $value->meeting)
                                                                                <option value={{$i}} selected>Pertemuan {{$i}}</option>
                                                                            @endif
                                                                        @endfor

                                                                    </select>
                                                                    <label>Nilai Respon</label>
                                                                    <input type="number" class="form-control" placeholder="Masukkan Nilai Respon" name="responsValue" value="{{$value->responsValue}}">
                                                                    <label>Nilai TP</label>
                                                                    <input type="number" class="form-control" placeholder="Masukkan Nilai TP" name="preliminaryAssignmentValue" value="{{$value->preliminaryAssignmentValue}}">
                                                                    <label>Nilai Praktikum</label>
                                                                    <input type="number" class="form-control" placeholder="Masukkan Nilai Praktikum" name="practiceValue" value="{{$value->practiceValue}}">
                                                                    <input type="hidden" name="student_id" value="{{$student->id}}">

                                                            </div>

                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Tambah</button>
                                                            </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                        </tr>
                                    </div>

                                @endforeach
                            </tbody>
                        </table>

                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            @endforeach
            <!-- /.col -->
            <div class="modal fade" id="modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Tambah Praktikan</h4>
                            </div>
                            <div class="modal-body">
                                <form class="" action="{{ route('app.master.myStudent.store')}}" method="post">
                                    {{ csrf_field() }}
                                    <label>Nama</label>
                                    <input type="text" class="form-control" placeholder="Masukkan Nama Praktikan" name="name">
                                    <label>Nim</label>
                                    <input type="text" class="form-control" placeholder="Masukkan Nim Praktikan" name="code">
                                    <label>Mata Kuliah</label>
                                    <input disabled type="number" class="form-control" placeholder="Masukkan Nilai Praktikum" name=course_id>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Tambah</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </div>
        @endsection

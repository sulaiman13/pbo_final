<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         if(!Schema::connection('Master')->hasTable('students')) {
             Schema::connection('Master')->create('students', function (Blueprint $table) {
                 $table->uuid('id')->primary();
                 $table->string('name');
                 $table->string('code');
                 $table->uuid('course_id');
                  $table->uuid('assistant_id');
                 $table->uuid('created_by');
                 $table->uuid('update_by');
                 $table->timestamps();
                 $table->softDeletes();
             });
             DB::statement('ALTER TABLE ONLY master.students ALTER COLUMN id SET DEFAULT uuid_generate_v4()');
         }
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::connection('Master')->drop('students');
     }
}

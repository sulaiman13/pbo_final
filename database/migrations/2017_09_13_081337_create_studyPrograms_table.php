<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudyProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         if(!Schema::connection('Reference')->hasTable('study_programs')) {
             Schema::connection('Reference')->create('study_programs', function (Blueprint $table) {
                 $table->uuid('id')->primary();
                 $table->string('code');
                 $table->string('name');
                 $table->uuid('created_by');
                 $table->uuid('update_by');
                 $table->timestamps();
                 $table->softDeletes();
             });
             DB::statement('ALTER TABLE ONLY reference.study_programs ALTER COLUMN id SET DEFAULT uuid_generate_v4()');
         }
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::connection('Reference')->drop('study_programs');
     }
}
